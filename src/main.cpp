#include <iostream>
#include "../include/Libnet.hpp"

int main(int argc, char *argv[])
{
    Libnet::Utils::DebugPrint dbg{};
    Libnet::Utils::HexConverter cvt{};

    if (argc < 2)
    {
        throw std::invalid_argument{"You did not provide a payload"};
    }

    dbg.printSuccess("Using libnet version: " + Libnet::Functions::getVersion().append("\n") );

    auto payload = reinterpret_cast<uint8_t *>(argv[1]);
    auto payloadSize = std::string{argv[1]}.length();

    dbg.printSuccess("[+] Payload: " +
                     std::string{argv[1]}
                         .append("\tpayload size: ")
                         .append(std::to_string(payloadSize))
                         .append("\n"));

    dbg.printWarning("[*] Initializing libnet ...\n");
    Libnet::LibnetContext context{Libnet::InjectionType::Raw4};
    dbg.printSuccess("[+] Successfully initialized libnet\n");

    dbg.printWarning("[*] Creating builder object\n");
    Libnet::Builder builder{context};

    dbg.printSuccess("[+] Seeding libnet random number generator\n");
    context.seedPrand();

    auto id32 = Libnet::Functions::getPrand(Libnet::Range::Pr16);
    auto id = static_cast<uint16_t>(id32);

    dbg.printSuccess("[+] Libnet random id: 0x" + cvt.fromInt(id).append("\n"));

    uint16_t seq{1};
    uint16_t totalPacketSize = LIBNET_IPV4_H + LIBNET_ICMPV4_ECHO_H;

    dbg.printSuccess("[*] Sending ICMP echo request to google.com\n");
    Libnet::IP destIp{context, "google.com", Libnet::IPType::V4};

    builder.buildIcmpV4Echo(ICMP_ECHO, 0x0, 0x0, id, seq, payload, payloadSize, 0x0);
    builder.autobuildIpV4(totalPacketSize, IPPROTO_ICMP, destIp);

    dbg.printSuccess("[+] Writing libnet packet to wire ...\n");
    context.write();
    context.clearPacket();

    dbg.printSuccess("[+] Done!!\n");

    return 0;
}