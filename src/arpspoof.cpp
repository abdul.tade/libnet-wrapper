#include "../include/Libnet.hpp"
#include <iostream>

using namespace Libnet;

/**
 * @brief Make sure libnet is opened
 * with the link type set to LinkType::Link
 * will fail otherwise
*/
void arpSend(LibnetContext &ctx, uint16_t arpOp)
{
    Buffer<byte> pkt{60};
    Builder builder{ctx};

    auto sha = ctx.getMacAddress();
    auto tha = MacAddr{"ff:ff:ff:ff:ff:ff"};
    auto spa = IP{ctx};
    auto tpa = IP{ctx, "127.0.0.1", IPType::V4};

    builder.autoBuildArp(arpOp,sha,spa,tha,tpa);
    builder.buildEthernet(tha,sha,ETHERTYPE_ARP,NULL,0,0);

    if (arpOp == ARPOP_REQUEST) {
        std::cout << tha << " who-has "
                  << tpa << " tell " << spa
                  << '\n';
    } else {
        std::cout << tha << " arp reply "
                  << spa << " is-at "
                  << sha << '\n';
    }

    ctx.write();
    ctx.clearPacket();
}
/**
 * Main Function
*/
int main(int argc, char *argv[])
{
    LibnetContext ctx{InjectionType::Link};
    while (true) {
        arpSend(ctx, ARPOP_REQUEST);
        sleep(2);
        arpSend(ctx,ARPOP_REPLY);
        sleep(2);
        ctx.clearPacket();
    }
}
