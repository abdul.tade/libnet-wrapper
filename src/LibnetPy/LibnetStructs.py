import ctypes

"""
struct libnet_in6_addr
{
    union
    {
        uint8_t   __u6_addr8[16];
        uint16_t  __u6_addr16[8];
        uint32_t  __u6_addr32[4];
    } __u6_addr;            /* 128-bit IP6 address */
};
"""

class U6Addr(ctypes.Union):

    _fields_ = [ 
        ("__u6_addr8", ctypes.Array())
    ]


class In6Addr(ctypes.Structure):
    _fields_ = [

    ]