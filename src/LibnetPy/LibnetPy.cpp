
#include <pybind11/pybind11.h>
#include "../include/Libnet.hpp"

namespace py = pybind11;
using namespace Libnet;

PYBIND11_MODULE(LibnetPy, m) {

    py::enum_<InjectionType>(m, "InjectionType")
        .value("Link", InjectionType::Link)
        .value("LinkAdv", InjectionType::LinkAdv)
        .value("Raw4", InjectionType::Raw4)
        .value("Raw4Adv", InjectionType::Raw4Adv)
        .value("Raw6", InjectionType::Raw6)
        .value("Raw6Adv", InjectionType::Raw6Adv)
        .export_values();

    py::enum_<Range>(m, "Range")
        .value("Pr2", Range::Pr2)
        .value("Pr8", Range::Pr8)
        .value("Pr16", Range::Pr16)
        .value("Pru16", Range::Pru16)
        .value("Pr32", Range::Pr32)
        .value("Pru32", Range::Pru32)
        .export_values();

    py::enum_<ChecksumMode>(m, "ChecksumMode")
        .value("On", ChecksumMode::On)
        .value("Off", ChecksumMode::Off)
        .export_values();

    py::enum_<ResolveMode>(m, "ResolveMode")
        .value("Resolve", ResolveMode::Resolve)
        .value("DontResolve", ResolveMode::DontResolve)
        .export_values();

    py::class_<LibnetStats>(m, "LibnetStats")
        .def(py::init<>())
        .def_readwrite("bytes_written", &LibnetStats::bytes_written)
        .def_readwrite("packet_errors", &LibnetStats::packet_errors)
        .def_readwrite("packets_sent", &LibnetStats::packets_sent);

    py::class_<LibnetContext>(m, "LibnetContext")
        .def(py::init<InjectionType,const std::string&>())
        .def("write", &LibnetContext::write)
        .def("clearPacket", &LibnetContext::clearPacket)
        .def("getStats", &LibnetContext::getStats)
        .def("getFileDescriptor", &LibnetContext::getFileDescriptor)
        .def("getDevice", &LibnetContext::getDevice)
        .def("getPacketSize", &LibnetContext::getPacketSize)
        .def("seedPrand", &LibnetContext::seedPrand)
        .def("toggleChecksum", &LibnetContext::toggleChecksum)
        .def("name2Addr4", &LibnetContext::name2Addr4);
}