#include <iostream>
#include <random>
#include "../include/Utils.hpp"

int main()
{
    using namespace Libnet;

    Buffer<int> buffer{256};
    auto alias = buffer + 10;
    auto alias_alias = alias - 100;

    for (size_t i = 0; i < buffer.size(); i++)
    {
        buffer[i] = rand()%buffer.size();
    }

    for (auto &c : buffer)
    {
        std::cout << c << '\n';
    }

    std::cout << "========================================\n";

    std::sort(buffer.begin(),buffer.end());

    for (auto &c : alias)
    {
        std::cout << c << '\n';
    }
}