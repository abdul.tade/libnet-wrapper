# pragma once

#include <exception>
#include <string>

namespace Libnet {

    namespace Exceptions
    {
        class InvalidMacAddress : public std::exception
        {

        public:

            InvalidMacAddress(const std::string &str)
                : _msg(str.c_str())
            {}

            const char* what() const noexcept override  {
                return _msg;
            }

        private:
            const char* _msg{};
        };

        class InvalidIPAddress : public std::exception
        {

        public:
            InvalidIPAddress(const std::string &str = "")
                : _msg(str == "" ? "Invalid ip address format" : str.c_str())
            {}

            const char* what() const noexcept override {
                return _msg;
            }

        private:
            const char* _msg{};
        };

        class LibnetContextError : public std::exception
        {

        public:

            LibnetContextError(const std::string &str = "")
                : _msg(str == "" ? "Libnet Context error" : str.c_str())
            {}

            const char* what() const noexcept override  {
                return _msg;
            }

        private:
            const char* _msg{};
        };

        class LibnetBuilderError : public std::exception
        {

        public:

            LibnetBuilderError(const std::string &str = "")
                : _msg(str == "" ? "Libnet Builder error" : str.c_str())
            {}

            const char* what() const noexcept override  {
                return _msg;
            }

        private:
            const char* _msg{};
        };

        class LibnetError : public std::exception
        {

        public:

            LibnetError(const std::string &str = "")
                : _msg(str == "" ? "Libnet error" : str.c_str())
            {}

            const char* what() const noexcept override  {
                return _msg;
            }

        private:
            const char* _msg{};
        };

        class PortListError : public std::exception
        {

        public:

            PortListError(const std::string &str = "")
                : _msg(str == "" ? "PortList error" : str.c_str())
            {}

            const char* what() const noexcept override  {
                return _msg;
            }

        private:
            const char* _msg{};
        };
    }

}