#pragma once

#include "Exceptions.hpp"
#include "LibnetContext.hpp"
#include "Utils.hpp"
#include <algorithm>
#include <cstdint>
#include <cstdlib>
#include <libnet.h>
#include <ostream>
#include <sstream>
#include <stdexcept>
#include <string>

namespace Libnet {

enum IPType : int { V4, V6 };

class IP {

public:

  static constexpr uint32_t ip_error = -1;

  IP(LibnetContext &ctx, const std::string &str, IPType iptype)
      : _ctx(ctx) 
  { 
        setIpParameters(ctx, str, iptype);
  }

  IP(LibnetContext &ctx) 
    : _ctx(ctx)
  {
    iptype = IPType::V4;
    auto addr = libnet_get_ipaddr4(_ctx._ctx);
    *this = addr;
  }

  IP(const IP &ip) 
    : _value(ip._value),
      _ctx(ip._ctx),
      _ipStr(ip._ipStr),
      iptype(ip.iptype)
  {}

 
  void operator=(const std::string &str) {
    this->setIpParameters(_ctx,str,iptype);
  }

  void operator=(uint32_t ipValue) {
    auto res = libnet_addr2name4(ipValue, ResolveMode::DontResolve);
    auto list = Utils::split(res,'.');
    ipFromList(list);
  }

  std::string toHostname() const {
    return { libnet_addr2name4(_value,ResolveMode::Resolve) };
  }

  friend std::ostream &operator<<(std::ostream &oss, const IP &ip) {
    oss << ip._ipStr;
    return oss;
  }

  friend struct LibnetContext;
  friend struct Builder;

private:

  uint32_t _value{};
  LibnetContext &_ctx;
  std::string _ipStr{};
  IPType iptype;

  void setIpParameters(LibnetContext &context, const std::string &str, IPType iptype) 
  {
    if (iptype == IPType::V6) {
      throw Exceptions::InvalidIPAddress{"IP version 6 is not supported"};
    }

    auto list = Utils::split(str, '.');
    if (list.size() != 4) {
      ipFromHostname(str);
    } 
    
    else {
        ipFromList(list);
    }

  }

  void ipFromList(std::vector<std::string> &list)
  {
    for (auto i = 0; i <= 3; ++i) {
      auto shiftValue = (i*8);
      _value = _value | (std::atoi(list[i].c_str()) << shiftValue);
    }
    _ipStr = Utils::join(".",list.begin(),list.end());
  }

  void ipFromHostname(const std::string &str)
  {
    _value = _ctx.name2Addr4(str, ResolveMode::Resolve);
    _ipStr = libnet_addr2name4(_value,ResolveMode::DontResolve);
    if (_value == IP::ip_error) {
      throw Exceptions::InvalidIPAddress{_ctx.getError()};
    }
  }

};

} // namespace Libnet