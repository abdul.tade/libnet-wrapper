#pragma once

#include <type_traits>
#include <vector>
#include <iostream>
#include <sstream>
#include <cmath>
#include <algorithm>
#include <map>
#include <compare>
#include <cstring>

#define DEFAULT_CONSTRUCTORS(ClassName, Tag) (      \
    ClassName(const ClassName &) = Tag;             \
    ClassName(ClassName &&) = Tag;                  \
    ClassName & operator=(const ClassName &) = Tag; \
    ClassName & operator=(ClassName &&) = Tag;)

namespace Libnet
{
    struct Utils
    {

        static std::vector<std::string> split(const std::string &str, char delimeter)
        {
            std::vector<std::string> tokens{};
            std::stringstream ss{str};

            for (std::string token{}; std::getline(ss, token, delimeter);)
            {
                tokens.emplace_back(token);
            }

            return tokens;
        }

        static std::string toLower(const std::string &str)
        {
            std::string res{str};
            std::transform(res.begin(), res.end(), res.begin(),
                           [](char c)
                           { return std::tolower(c); });
            return res;
        }

        template <typename T>
        static void swap(T &x, T &y)
        {
            auto tmp = x;
            x = y;
            y = tmp;
        }

        template <typename Iter>
        static std::string join(const std::string &delimeter, Iter start, Iter end)
        {
            std::string result{};
            std::stringstream ss{};
            auto size = static_cast<unsigned long>(end - start);

            for (auto it = start; it != end; ++it)
            {
                ss << *it;
                auto i = static_cast<unsigned long>(it - start);
                if (i == size - 1)
                {
                    result.append(ss.str());
                }
                else
                {
                    result.append(ss.str() + delimeter);
                    ss.str("");
                }
            }

            return result;
        }

        struct HexConverter
        {

            template <typename T>
            std::string fromInt(T value)
            {
                std::stringstream ss{};
                ss << std::hex << value;
                auto retStr = ss.str();
                return retStr.length() < 2 ? "0" + retStr : retStr;
            }

            template <typename T>
            std::enable_if_t<std::is_integral<T>::value, T>
            toInt(const std::string &str)
            {

                auto value = 0ull;
                auto S = str;
                auto tag = S.substr(0, 2);

                if (tag == "0x" or tag == "0X")
                {
                    S = S.substr(2, S.length());
                }

                S = Utils::toLower(S);
                auto length = S.length();

                for (auto i = 0ul; i < length; ++i)
                {
                    if (not m.count(S[i]))
                    {
                        throw std::invalid_argument{"Unrecognized hex character"};
                    }
                    else
                    {
                        auto powOf16 = static_cast<size_t>(std::pow(16ul, (length - i - 1)));
                        value += m[S[i]] * powOf16;
                    }
                }
                return value;
            }

        private:
            std::map<char, uint64_t> m{
                {'0', 0}, {'1', 1}, {'2', 2}, {'3', 3}, {'4', 4}, {'5', 5}, {'6', 6}, {'7', 7}, {'8', 8}, {'9', 9}, {'a', 10}, {'b', 11}, {'c', 12}, {'d', 13}, {'e', 14}, {'f', 15}};
        };

        struct DebugPrint
        {

            void printSuccess(const std::string &str) const
            {
                std::cout << Green << "DEBUG: " << str << NoColor;
            }

            void printError(const std::string &str)
            {
                std::cout << Red << "DEBUG: " << str << NoColor;
            }

            void printWarning(const std::string &str)
            {
                std::cout << Yellow << "DEBUG: " << str << NoColor;
            }

        private:
            const std::string Red = "\033[31m";
            const std::string Green = "\033[32m";
            const std::string Yellow = "\033[33m";
            const std::string NoColor = "\033[0m";
        };
    };

#include <iostream>
#include <cstring>
#include <compare>

    template <typename ElementType>
    /**
     * @brief Class to manage heap allocated buffer
     * Not MT safe.
     */
    class Buffer
    {
    public:
        struct iterator
        {
            using iterator_category = std::random_access_iterator_tag;
            using value_type = ElementType;
            using pointer = ElementType *;
            using reference = ElementType &;
            using difference_type = ptrdiff_t;

            iterator(ElementType *ptr)
                : ptr_(ptr)
            {
            }

            reference operator*()
            {
                return *ptr_;
            }

            pointer operator->()
            {
                return ptr_;
            }

            iterator &operator++()
            {
                ++ptr_;
                return *this;
            }

            iterator operator++(int)
            {
                auto tmp = *this;
                ++(*this);
                return tmp;
            }

            iterator &operator--()
            {
                --ptr_;
                return *this;
            }

            iterator operator--(int)
            {
                auto tmp = *this;
                --(*this);
                return tmp;
            }

            iterator operator+(ptrdiff_t offset)
            {
                return iterator{ptr_ + offset};
            }

            iterator operator-(ptrdiff_t offset)
            {
                return iterator{ptr_ + offset};
            }

            ptrdiff_t operator-(const iterator &it)
            {
                return static_cast<ptrdiff_t>(ptr_ - it.ptr_);
            }

            iterator &operator+=(ptrdiff_t offset)
            {
                ptr_ += offset;
                return *this;
            }

            iterator &operator-=(ptrdiff_t offset)
            {
                ptr_ -= offset;
                return *this;
            }

            std::strong_ordering operator<=>(const iterator &it) const noexcept = default;

        private:
            ElementType *ptr_;
        };
        /**
         * use for places that accepts forward iterators
         * */
        iterator begin()
        {
            return iterator{buff_};
        }
        /**
         * use for places that accepts forward iterators
         * */
        iterator end()
        {
            return iterator{buff_ + size_};
        }

        /**
         * used by decrementable iterator
        */
        iterator rbegin()
        {
            return iterator{buff_ + size_ - 1};
        }

        /**
         * used by decrementable iterators
        */
        iterator rend()
        {
            return iterator{buff_ - 1};
        }

        Buffer(size_t size)
            : size_(size),
              isOwner_(true)
        {
            if (size == 0)
            {
                throw std::invalid_argument{"Cannot allocate a buffer of zero size"};
            }
            buff_ = new ElementType[size];
        }

        Buffer(ElementType *buff, size_t size)
        {
            if (buff_ == nullptr)
            {
                throw std::invalid_argument{"Cannot copy null buffer"};
            }
            buff_ = new ElementType[size];
            size_ = size;
            isOwner_ = true;
            std::copy(buff, buff + size, buff_);
        }

        ~Buffer()
        {
            if (isOwner_)
            {
                if (buff_ != nullptr)
                {
                    delete[] buff_;
                }
            }
        }

        /**
         * @brief Creates an alias
         */
        Buffer(const Buffer &buffer)
            : size_(buffer.size_),
              buff_(buffer.buff_),
              isOwner_(false)
        {
        }

        Buffer operator=(const Buffer &buffer)
        {
            return {buffer};
        }

        /**
         * @brief Performs deep copy of memory
         */
        Buffer deepCopy()
        {
            return {buff_, size_};
        }

        void zeros() const noexcept
        {
            std::memset(buff_, 0x0, sizeof(ElementType) * size_);
        }

        size_t size() const noexcept
        {
            return size_;
        }

        bool operator==(const Buffer &buffer)
        {
            if (size_ != buffer.size_)
            {
                return false;
            }
            return std::equal(buff_, buff_ + size_, buffer.buff_);
        }

        bool operator!=(const Buffer &buffer)
        {
            return not operator==(buffer);
        }

        Buffer operator+(long offset)
        {
            return applyOffset(offset);
        }

        Buffer operator-(long offset)
        {
            if (isOwner_ and (offset > 0)) {
                throw std::invalid_argument{"offset would cause buffer underflow"};
            }
            return applyOffset(offset);
        }

        long operator-(const Buffer &buffer)
        {
            if (&buffer == this)
            {
                return 0;
            }
            else
            {
                return static_cast<long>(buff_ - buffer.buff_);
            }
        }

        friend Buffer operator+(ptrdiff_t offset, const Buffer &buffer)
        {
            return buffer.applyOffset(offset);
        }

        ElementType &operator[](size_t index)
        {
            if (index > size_ - 1)
            {
                throw std::out_of_range{"index out of range"};
            }
            return buff_[index];
        }

        /**
         * @returns Returns pointer to internal data
         * provided to interface with C api's
         */
        ElementType *data()
        {
            return buff_;
        }

        friend struct LibnetContext;

    private:
        size_t size_{};
        ElementType *buff_;
        bool isOwner_{false};

        Buffer applyOffset(long offset)
        {
            if (isOwner_)
            {
                if (not(offset < 0))
                {
                    if (offset > size_ - 1)
                    {
                        throw std::out_of_range{"offset would lead to buffer overflow"};
                    }
                    auto off = size_ - static_cast<size_t>(offset);
                    buff_ += offset;
                    size_ -= offset;
                    auto res = Buffer{*this};
                    buff_ -= offset;
                    size_ += offset;
                    return res;
                }
                else
                {
                    throw std::out_of_range{"offset would lead to buffer underflow"};
                }
            }
            else
            {
                buff_ += offset;
                size_ -= offset;
                auto res = Buffer{*this};
                buff_ -= offset;
                size_ += offset;
                return res;
            }
        }
    };
}