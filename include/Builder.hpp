#pragma once

#include "LibnetContext.hpp"
#include "IP.hpp"
#include "MacAddr.hpp"

namespace Libnet
{

    using Port = uint16_t;
    using VlanId = uint16_t;
    using Payload = const uint8_t *;

    struct Builder
    {
        Builder(LibnetContext &ctx)
            : _ctx(ctx)
        {
        }

        /**
         * @brief Builds Vlan tagging header
         */
        Ptag build8021q(MacAddr &dst, MacAddr &src, uint16_t tagProtoId,
                        uint8_t priority, uint8_t canFmtId, VlanId vlanId,
                        uint16_t lenProto, Payload payload, uint32_t payloadSize, Ptag ptag)
        {
            auto tag = libnet_build_802_1q(dst._macAddr, src._macAddr, tagProtoId, priority, canFmtId, vlanId,
                                           lenProto, payload, payloadSize, _ctx._ctx, ptag);
            checkError(tag);
            return tag;
        }

        /**
         * @brief builds an extended authentication protocol header
         * */
        Ptag build8021x(uint8_t EapVersion, uint8_t EapType, uint16_t Length,
                        Payload payload, uint32_t payloadSize, Ptag ptag)
        {
            auto tag = libnet_build_802_1x(EapVersion, EapType, Length, payload, payloadSize, _ctx._ctx, ptag);
            checkError(tag);
            return tag;
        }

        Ptag build8022(uint8_t Dsap, uint8_t Ssap, uint8_t Control,
                       Payload payload, uint32_t payloadSize, Ptag ptag)
        {
            auto tag = libnet_build_802_2(Dsap, Ssap, Control, payload, payloadSize, _ctx._ctx, ptag);
            checkError(tag);
            return tag;
        }

        Ptag build8022Snap(uint8_t dsap, uint8_t ssap, uint8_t control,
                           uint8_t *oui, uint16_t type, Payload payload, uint32_t payloadSize,
                           Ptag ptag)
        {
            auto tag = libnet_build_802_2snap(dsap, ssap, control,
                                              oui, type, payload, payloadSize,
                                              _ctx._ctx, ptag);
            checkError(tag);
            return tag;
        }

        Ptag build8023(MacAddr &dst, MacAddr &src, uint16_t len,
                       Payload payload, uint32_t payloadSize, Ptag ptag)
        {
            auto tag = libnet_build_802_3(dst._macAddr, src._macAddr, len, payload, payloadSize, _ctx._ctx, ptag);
            checkError(tag);
            return tag;
        }

        Ptag buildEthernet(MacAddr &dst, MacAddr &src, uint16_t type,
                           Payload payload, uint32_t payloadSize, Ptag ptag)
        {
            auto tag = libnet_build_ethernet(dst._macAddr, src._macAddr, type, payload, payloadSize, _ctx._ctx, ptag);
            checkError(tag);
            return tag;
        }

        Ptag autoBuildEthernet(MacAddr &dst, uint16_t type)
        {
            auto tag = libnet_autobuild_ethernet(dst._macAddr, type, _ctx._ctx);
            checkError(tag);
            return tag;
        }

        Ptag buildFddi(uint8_t fc, MacAddr &dst, MacAddr &src, uint8_t dsap,
                       uint8_t ssap, uint8_t cf, const uint8_t *oui, uint16_t type, Payload payload,
                       uint32_t payloadSize, Ptag ptag)
        {
            auto tag = libnet_build_fddi(fc, dst._macAddr, src._macAddr, dsap, ssap, cf, oui, type, payload, payloadSize, _ctx._ctx, ptag);
            checkError(tag);
            return tag;
        }

        Ptag autoBuildFddi(uint8_t fc, MacAddr &dst, uint8_t dsap, uint8_t ssap,
                           uint8_t cf, const uint8_t *oui, uint16_t type)
        {
            auto tag = libnet_autobuild_fddi(fc, dst._macAddr, dsap, ssap, cf, oui, type, _ctx._ctx);
            checkError(tag);
            return tag;
        }

        /**
         * Builds an Address Resolution Protocol (ARP) header.  Depending on the op
         * value, the function builds one of several different types of RFC 826 or
         * RFC 903 RARP packets.
         * @param hrd hardware address format
         * @param pro protocol address format
         * @param hln hardware address length
         * @param pln protocol address length
         * @param op ARP operation type
         * @param sha sender's hardware address
         * @param spa sender's protocol address
         * @param tha target hardware address
         * @param tpa target protocol address
         * @param payload optional payload or NULL
         * @param payloadSize payload length or 0
         * @param ptag protocol tag to modify an existing header, 0 to build a new one
         * @return protocol tag value on success
         * @retval -1 on error
         */
        Ptag buildArp(uint16_t hrd, uint16_t pro, uint8_t hln, uint8_t pln,
                      uint16_t op, const uint8_t *sha, const uint8_t *spa, const uint8_t *tha, const uint8_t *tpa,
                      const uint8_t *payload, uint32_t payloadSize, Ptag ptag)
        {
            auto tag = libnet_build_arp(hrd, pro, hln, pln, op, sha, spa, tha, tpa, payload, payloadSize, _ctx._ctx, ptag);
            checkError(tag);
            return tag;
        }

        /**
         * Autouilds an Address Resolution Protocol (ARP) header.  Depending on the op
         * value, the function builds one of several different types of RFC 826 or
         * RFC 903 RARP packets.
         * @param op ARP operation type
         * @param sha sender's hardware address
         * @param spa sender's protocol address
         * @param tha target hardware address
         * @param tpa target protocol address
         * @return protocol tag value on success
         * @retval -1 on error
         */
        Ptag autoBuildArp(uint16_t op, MacAddr &sha, IP &spa, MacAddr &tha, IP &tpa)
        {
            byte *_spa = reinterpret_cast<byte *>(&spa._value);
            byte *_tpa = reinterpret_cast<byte *>(&tpa._value);
            auto tag = libnet_autobuild_arp(op, sha._macAddr, _spa, tha._macAddr, _tpa, _ctx._ctx);
            checkError(tag);
            return tag;
        }

        /**
         * Builds an RFC 793 Transmission Control Protocol (TCP) header.
         * @param sp source port
         * @param dp destination port
         * @param seq sequence number
         * @param ack acknowledgement number
         * @param control control flags
         * @param win window size
         * @param sum checksum (0 for libnet to auto-fill)
         * @param urg urgent pointer
         * @param len total length of the TCP packet (for checksum calculation)
         * @param payload
         * @param payloadSize payload length or 0
         * @param ptag protocol tag to modify an existing header, 0 to build a new one
         * @return protocol tag value on success
         * @retval -1 on error
         */
        Ptag buildTcp(Port sp, Port dp, uint32_t seq, uint32_t ack,
                      uint8_t control, uint16_t win, uint16_t sum, uint16_t urg, uint16_t len,
                      Payload payload, uint32_t payloadSize, Ptag ptag)
        {
            auto tag = libnet_build_tcp(sp, dp, seq, ack, control, win, sum, urg, len, payload, payloadSize, _ctx._ctx, ptag);
            checkError(tag);
            return tag;
        }

        Ptag buildTcpOptions(const uint8_t *options, uint32_t optionSize, Ptag ptag)
        {
            auto tag = libnet_build_tcp_options(options, optionSize, _ctx._ctx, ptag);
            checkError(tag);
            return tag;
        }

        Ptag buildUdp(Port sp, Port dp, uint16_t len, uint16_t sum, Payload payload, uint32_t payloadSize, Ptag ptag)
        {
            auto tag = libnet_build_udp(sp, dp, len, sum, payload, payloadSize, _ctx._ctx, ptag);
            checkError(tag);
            return tag;
        }

        Ptag buildCdp(uint8_t version, uint8_t ttl, uint16_t sum, uint16_t type,
                      uint16_t value_s, const uint8_t *value, const uint8_t *payload, uint32_t payload_s,
                      Ptag ptag)
        {
            auto tag = libnet_build_cdp(version, ttl, sum, type, value_s, value, payload, payload_s, _ctx._ctx, ptag);
            checkError(tag);
            return tag;
        }

        Ptag buildIpV4(uint16_t ipLen, uint8_t tos, uint16_t id, uint16_t frag,
                       uint8_t ttl, uint8_t prot, uint16_t sum, IP &src, IP &dst,
                       Payload payload, uint32_t payloadSize, Ptag ptag)
        {
            auto tag = libnet_build_ipv4(ipLen, tos, id, frag, ttl, prot, sum, src._value, dst._value, payload, payloadSize, _ctx._ctx, ptag);
            checkError(tag);
            return tag;
        }

        Ptag buildIpV4Options(const uint8_t *options, uint32_t optionSize, Ptag ptag)
        {
            auto tag = libnet_build_ipv4_options(options, optionSize, _ctx._ctx, ptag);
            checkError(tag);
            return tag;
        }

        Ptag autobuildIpV4(uint16_t len, uint8_t prot, IP &dst)
        {
            auto tag = libnet_autobuild_ipv4(len, prot, dst._value, _ctx._ctx);
            checkError(tag);
            return tag;
        }

        Ptag buildIcmpV4Echo(uint8_t type, uint8_t code, uint16_t sum,
                             uint16_t id, uint16_t seq, Payload payload, uint32_t payloadSize,
                             Ptag ptag)
        {
            auto tag = libnet_build_icmpv4_echo(type, code, sum, id, seq, payload, payloadSize, _ctx._ctx, ptag);
            checkError(tag);
            return tag;
        }

        Ptag buildIcmpV4Mask(uint8_t type, uint8_t code, uint16_t sum,
                             uint16_t id, uint16_t seq, uint32_t mask, const uint8_t *payload,
                             uint32_t payload_s, Ptag ptag)
        {
            auto tag = libnet_build_icmpv4_mask(type, code, sum, id, seq, mask, payload, payload_s, _ctx._ctx, ptag);
            checkError(tag);
            return tag;
        }

        Ptag buildIcmpV4Unreach(uint8_t type, uint8_t code, uint16_t sum,
                                const uint8_t *payload, uint32_t payload_s, Ptag ptag)
        {
            auto tag = libnet_build_icmpv4_unreach(type,code,sum,payload,payload_s,_ctx._ctx,ptag);
            checkError(tag);
            return tag;
        }

    private:
        LibnetContext &_ctx;

        void checkError(Ptag tag)
        {
            if (tag == -1)
            {
                throw Exceptions::LibnetBuilderError{std::string{_ctx.getError()}};
            }
        }
    };
}