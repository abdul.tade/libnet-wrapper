# pragma once

# include "LibnetContext.hpp"
# include "Builder.hpp"
# include "Utils.hpp"
# include "IP.hpp"
# include "MacAddr.hpp"
# include "Exceptions.hpp"
# include "PortList.hpp"

namespace Libnet {

    struct Functions {

        static uint32_t getPrand(Range range) {
            auto res = libnet_get_prand(range);
            if (res == -1) {
                throw Exceptions::LibnetError{__func__ + std::string{"Range value error"}};
            }
            return res;
        }

        static std::string addr2Name4(uint32_t in, ResolveMode mode) {
            return { libnet_addr2name4(in, mode) };
        }

        static int in6IsError(In6Addr &addr) {
            return libnet_in6_is_error(addr);
        }

        static void addr2Name6R(In6Addr &addr, ResolveMode mode, const std::string &hostname, int hostnameLen) {
            libnet_addr2name6_r(addr, mode, const_cast<char*>(hostname.c_str()), hostnameLen);
        }

        static std::string getVersion() {
            return { libnet_version() };
        }
    };
}