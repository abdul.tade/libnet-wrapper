# pragma once

#include "Exceptions.hpp"
#include "LibnetContext.hpp"
#include "Utils.hpp"
#include <algorithm>
#include <iostream>
#include <stdexcept>
#include <vector>

namespace Libnet {

using byte = unsigned char;
/**
 * Implement checking for invalid mac octet range
 */
class MacAddr {

public:
  constexpr static size_t addr_length = 6;

  MacAddr(const std::string &mAddr) {

    auto list = Utils::split(mAddr, ':');
    std::transform(list.begin(), list.end(), list.begin(),
                   [](std::string str) { return Utils::toLower(str); });
   
    if (list.size() != 6) {
      throw Exceptions::InvalidMacAddress("Invalid mac address format");
    }

    for (auto i = 0ul; i < list.size() ; ++i) {
      _macAddr[i] = _cvt.toInt<byte>(list[i]);
    }

    makeMacString();
  }  

  MacAddr(Libnet::byte* addr)
  {
    std::copy(addr, addr+MacAddr::addr_length, _macAddr);
    makeMacString();
  }

  MacAddr(const MacAddr &) = default;
  MacAddr &operator=(const MacAddr &) = default;
  MacAddr(MacAddr &&) = default;
  MacAddr &operator=(MacAddr &&) = default;

  bool operator==(const MacAddr &other) {
    return std::equal(_macAddr, _macAddr + MacAddr::addr_length, other._macAddr);
  }

  bool operator!=(const MacAddr &other) { 
    return not operator==(other); 
  }

  friend std::ostream& operator<<(std::ostream &oss, const MacAddr &addr) {
    return oss << addr._macStr;
  }

  byte* data() {
    return _macAddr;
  }

  friend class LibnetContext;
  friend class Builder;

private:

  byte _macAddr[6];
  std::string _macStr{};
  Utils::HexConverter _cvt{};

  void makeMacString() 
  {
    std::vector<std::string> arrAddr {
      _cvt.fromInt<int>(_macAddr[0]), _cvt.fromInt<int>(_macAddr[1]),
      _cvt.fromInt<int>(_macAddr[2]), _cvt.fromInt<int>(_macAddr[3]),
      _cvt.fromInt<int>(_macAddr[4]), _cvt.fromInt<int>(_macAddr[5])
    };
    _macStr = Utils::join(":", arrAddr.begin(), arrAddr.end());
  }

};

} // namespace Libnet