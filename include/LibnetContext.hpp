#pragma once

#include <string>
#include <libnet.h>
#include <array>
#include <stdexcept>
#include "Exceptions.hpp"
#include "MacAddr.hpp"

namespace Libnet
{
    enum InjectionType : int
    {
        Link = LIBNET_LINK,
        LinkAdv = LIBNET_LINK_ADV,
        Raw4 = LIBNET_RAW4,
        Raw4Adv = LIBNET_RAW4_ADV,
        Raw6 = LIBNET_RAW6,
        Raw6Adv = LIBNET_RAW6_ADV
    };

    enum Range : int
    {
        Pr2 = LIBNET_PR2,
        Pr8 = LIBNET_PR8,
        Pr16 = LIBNET_PR16,
        Pru16 = LIBNET_PRu16,
        Pr32 = LIBNET_PR32,
        Pru32 = LIBNET_PRu32
    };

    enum ChecksumMode : int
    {
        On = LIBNET_ON,
        Off = LIBNET_OFF
    };

    enum LibnetStatus : int
    {
        Success = 1,
        Error = -1
    };

    enum ResolveMode : uint8_t
    {
        Resolve = LIBNET_RESOLVE,
        DontResolve = LIBNET_DONT_RESOLVE
    };

    using LibnetStats = struct libnet_stats;
    using FileDescriptor = int;
    using In6Addr = struct libnet_in6_addr;
    using Plist = libnet_plist_t;
    using Ptag = libnet_ptag_t;
    using PBlock = struct libnet_block_t;

    struct LibnetContext
    {

        LibnetContext(InjectionType InjType, std::string const &device = "")
        {
            auto deviceString = (device == "") ? nullptr : device.c_str();
            _ctx = libnet_init(InjType, deviceString, _errBuff);
            if (_ctx == nullptr)
            {
                throw Exceptions::LibnetContextError{std::string{_errBuff, 256}};
            }
        }

        ~LibnetContext()
        {
            if (_ctx != nullptr)
            {
                libnet_destroy(_ctx);
            }
        }

        LibnetContext(const LibnetContext &) = delete;
        LibnetContext &operator=(const LibnetContext &) = delete;
        LibnetContext(LibnetContext &&) = delete;
        LibnetContext &operator=(LibnetContext &&) = delete;

        int write()
        {
            auto bytesCount = libnet_write(_ctx);
            if (bytesCount == -1)
            {
                throw Exceptions::LibnetContextError{getError()};
            }
            return bytesCount;
        }

        void clearPacket()
        {
            libnet_clear_packet(_ctx);
        }

        /**
         * @brief get statistics for packets transmitted or lost
         */
        LibnetStats getStats()
        {
            LibnetStats stats{};
            libnet_stats(_ctx, &stats);
            return stats;
        }

        FileDescriptor getFileDescriptor()
        {
            return libnet_getfd(_ctx);
        }

#ifdef SO_SNDBUF
        /**
         * Tries to set the TX buffer size to a max_bytes value
         * @param max_bytes new TX buffer size
         * @return 0 on success, -1 on failure
         */
        int setMaxBufferSize(int max_bytes)
        {
            return -1;
        }
#endif

        std::string getDevice()
        {
            auto name = libnet_getdevice(_ctx);
            if (name == nullptr)
            {
                return std::string{""};
            }
            return std::string{name};
        }

        uint8_t *getPbuf(libnet_ptag_t ptag)
        {
            auto result = libnet_getpbuf(_ctx, ptag);
            if (result == nullptr)
            {
                throw std::runtime_error{this->getError()};
            }
            return result;
        }

        uint32_t getPbufSize(libnet_ptag_t ptag)
        {
            return libnet_getpbuf_size(_ctx, ptag);
        }

        uint32_t getPacketSize()
        {
            return libnet_getpacket_size(_ctx);
        }

        void seedPrand()
        {
            auto res = libnet_seed_prand(_ctx);
            if (res == -1)
            {
                throw Exceptions::LibnetContextError{getError()};
            }
        }

        void toggleChecksum(libnet_ptag_t ptag, ChecksumMode mode)
        {
            auto res = libnet_toggle_checksum(_ctx, ptag, mode);
            if (res == -1)
            {
                throw Exceptions::LibnetContextError{getError()};
            }
        }

        uint32_t name2Addr4(const std::string &hostname, ResolveMode mode)
        {
            auto res = libnet_name2addr4(_ctx, const_cast<char *>(hostname.c_str()), mode);
            if (res == -1)
            {
                throw Exceptions::LibnetContextError{getError()};
            }
            return res;
        }

        In6Addr name2Addr6(const std::string &hostname, ResolveMode mode)
        {
            return libnet_name2addr6(_ctx, hostname.c_str(), mode);
        }

        MacAddr getMacAddress()
        {
            auto addr = libnet_get_hwaddr(_ctx);
            if (addr == nullptr)
            {
                throw Exceptions::LibnetContextError{__func__ + getError()};
            }
            return {addr->ether_addr_octet};
        }

        void pblockCoalesce(Buffer<byte> &buffer)
        {
            auto res = libnet_pblock_coalesce(_ctx, &buffer.buff_, reinterpret_cast<uint32_t *>(&buffer.size_));
            if (res < 0) {
                throw Exceptions::LibnetContextError{getError()};
            }
        }

#if defined(__WIN32__)

        /**
         * [Internal]
         */
        MacAddr win32GetRemoteMac(const IP &ip)
        {
            byte *mac = libnet_win32_get_remote_mac(_ctx._ctx, ip._value);
            if (mac == nullptr)
            {
                throw Exceptions::LibnetContextError{__func__ + getError()};
            }
            return {mac};
        }

        /**
         * [Internal]
         */
        void closeLinkInterface()
        {
            if (libnet_close_link_interface(_ctx._ctx) == -1)
            {
                throw Exceptions::LibnetContextError(__func__ + getError());
            }
        }

        /**
         * [Internal]
         */
        byte *win32ReadArpTable(const IP &ip)
        {
            auto res = libnet_win32_read_arp_table(ip._value);
            if (res == nullptr)
            {
                throw Exceptions::LibnetContextError{__func__ + getError()};
            }
        }

        /**
         * @brief checks if an iface exists or is up
         * @returns a boolean, if false call .getError()
         * might be due to ioctl or socket error
         */
        bool checkInterface()
        {
            auto res = libnet_check_interface(_ctx._ctx);
            return res < 0;
        }

#endif

        std::string getError()
        {
            return std::string{libnet_geterror(_ctx)};
        }

        friend struct Builder;
        friend class IP;
        friend struct PortList;

    private:
        char _errBuff[256]{0};
        libnet_t *_ctx{};
    };

} // namespace libnet
