# pragma once

# include "LibnetContext.hpp"
# include <tuple>

namespace Libnet {

    using Port = uint16_t;

    struct PortList {

        PortList(LibnetContext &ctx, const std::string &tokenList)
            : _ctx(ctx),
            _tokenList(tokenList)
        {
            auto res = libnet_plist_chain_new(_ctx._ctx, &_plist, const_cast<char*>(tokenList.c_str()));
            if (res == -1) {
                throw Exceptions::PortListError{_ctx.getError()};
            }
        }

        ~PortList() noexcept(false)
        {
            if (_plist != nullptr) {
                auto res = libnet_plist_chain_free(_plist);
                if (res == -1) {
                    throw Exceptions::PortListError{__func__ + std::string{"() error freeing port list chain"}};
                }
            }
        }


        std::tuple<Port, Port> next() {
            Port bport{}, eport{};
            if (not _isDone) {
                auto res = libnet_plist_chain_next_pair(_plist, &bport, &eport);
                if (res == -1) {
                    throw Exceptions::PortListError{__func__ + std::string{"() failed to read port list pair"}};
                }
                _isDone = (res == 0);
            }
            return { bport, eport };
        }

        bool isEnd() const 
        {
            return _isDone;
        }


    private:
        LibnetContext &_ctx;
        std::string _tokenList{};
        libnet_plist_t* _plist{};
        bool _isDone{false};
    };

}